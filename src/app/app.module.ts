import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms'
import { HttpModule, JsonpModule } from '@angular/http';
import { routing, appRoutingProviders } from './app.routing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CdkTableModule } from '@angular/cdk/table';
import {
  MatDialogModule,
  MatButtonModule, 
  MatCheckboxModule,
  MatFormFieldModule,
  MatInputModule,
  MatDialogConfig
} from '@angular/material';

import { AppComponent } from './app.component';
import { LoginComponent } from './components/login.component';
import { ProcesoComponent } from './components/proceso.component';
import { DetalleComponent } from './components/detalle.component';
import { IndicadoresComponent } from './components/indicadores.component';
import { AddUserComponent } from './components/add-user.component';
import { ListUsersComponent } from './components/list-users.component';
import { EditUserComponent } from './components/edit-user.component';
import { AddPlantillaComponent } from './components/add-plantilla.component';
import { StructProcComponent } from './components/estructura-proceso.component';
import { AddProcesoComponent } from './components/add-proceso.component';
import { StructIndicComponent } from './components/estructura-indicadores.component';
import { EditPlantillaComponent } from './components/edit-planitlla.component';
import { ProcesoViewComponent } from './components/proceso-view.component';
import { DialogAlert } from './components/dialog-alert';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    ProcesoComponent,
    DetalleComponent,
    IndicadoresComponent,
    AddUserComponent,
    ListUsersComponent,
    EditUserComponent,
    AddPlantillaComponent,
    StructProcComponent,
    AddProcesoComponent,
    StructIndicComponent,
    EditPlantillaComponent,
    ProcesoViewComponent,
    DialogAlert
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    routing,
    BrowserAnimationsModule, 
    MatButtonModule, 
    MatCheckboxModule,
    MatFormFieldModule,
    MatInputModule,
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    MatDialogModule
  ],
  providers: [],
  bootstrap: [AppComponent],
  exports: [
    DialogAlert
  ],
  entryComponents: [
      DialogAlert
  ],
})
export class AppModule { }
