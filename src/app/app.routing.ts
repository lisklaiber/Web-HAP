import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoginComponent } from './components/login.component';
import { ProcesoComponent } from './components/proceso.component';
import { DetalleComponent } from './components/detalle.component';
import { IndicadoresComponent } from './components/indicadores.component';
import { AddUserComponent } from './components/add-user.component';
import { ListUsersComponent } from './components/list-users.component';
import { EditUserComponent } from './components/edit-user.component';
import { AddPlantillaComponent } from './components/add-plantilla.component';
import { AddProcesoComponent } from './components/add-proceso.component';
import { EditPlantillaComponent } from './components/edit-planitlla.component';

const appRoutes: Routes = [
	{path: '', component: LoginComponent},
	{path: 'login', component: LoginComponent},
	{path: 'proceso', component: ProcesoComponent},
	{path: 'detalle', component: DetalleComponent},
	{path: 'indicadores', component: IndicadoresComponent},
	{path: 'admin/user', component: AddUserComponent},
	{path: 'admin/list-user', component: ListUsersComponent},
	{path: 'admin/edit-user', component: EditUserComponent},
	{path: 'admin/plantilla', component: AddPlantillaComponent},
	{path: 'admin/edit-plantilla', component: EditPlantillaComponent},
	{path: 'admin/proceso', component: AddProcesoComponent},
	{path: '**', component: LoginComponent}
];

export const appRoutingProviders: any[] = [];
export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);