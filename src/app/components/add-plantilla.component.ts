import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

//import { AutenticationService } from '../services/autentication.service';
//import { Autentication } from '../models/autentication';

@Component({
	selector: 'add-plantilla',
	templateUrl: '../views/add-plantilla.html',
	styleUrls: ['../../assets/css/add-plantilla.css', 
		'../../assets/css/flujo.css', 
		'../../assets/css/add-user.css', 
		'../../assets/css/edit-user.css'],
	providers: []
})

export class AddPlantillaComponent implements OnInit{
	public paso: number;

	constructor(

	){
		this.paso = 1;
	}

	ngOnInit(){
		document.body.style.backgroundColor = "#FFF";
	}

	Avanzar(){
		this.paso++;
	}
	
	Retroceder(){
		this.paso--;
	}
}
