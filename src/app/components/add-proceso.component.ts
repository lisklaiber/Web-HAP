import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

/*Services*/
import { AddProcesoService } from '../services/add-proceso.service';
import { PlantillaService } from '../services/plantilla.service';

/*Models*/
import { Proceso } from '../models/proceso';
import { MacroProceso } from '../models/macro-proceso';
import { Template } from '../models/plantilla';

/*PopUp's*/
import { DialogAlert } from './dialog-alert';

@Component({
	selector: 'add-proceso',
	templateUrl: '../views/add-proceso.html',
	styleUrls: ['../../assets/css/add-plantilla.css', 
		'../../assets/css/flujo.css', 
		'../../assets/css/add-user.css', 
		'../../assets/css/edit-user.css',
		'../../assets/css/add-proceso.css'],
	providers: [AddProcesoService, PlantillaService]
})

export class AddProcesoComponent implements OnInit{
	public paso: number;
	public errorMessage: any;
	public templateSelected: any;
	private proceso: Proceso[] = [];
	private macroProceso: MacroProceso;
	private idEmpresa: number;
	private templates: Template[];

	constructor(
		private _AddProcesoService: AddProcesoService,
		private _route: ActivatedRoute,
		private _router: Router,
		public dialog: MatDialog,
		private _PlantillaService: PlantillaService
	){
		this.paso = 1;
		this.macroProceso = new MacroProceso("", "", 1);
		this.idEmpresa = 1;
	}

	ngOnInit(){
		document.body.style.backgroundColor = "#FFF";
	}

	Avanzar(){
		this.paso++;
	}

	ValidarProceso(){

		if(this.macroProceso.Nombre === ""){
			this.openDialog("Debe ingresar un nombre");
		}else{
			this._AddProcesoService.ValidarProceso(this.macroProceso.Nombre, this.macroProceso.ID_Empresa).subscribe(
				result => {
					if(result.resp == "Aprobado"){
						this.paso++;
						this.LoadTemplates();
					}else{
						this.openDialog(result.resp);
					}
				},
				error => {
					this.errorMessage = <any>error;
					if(this.errorMessage != null){
						console.log(this.errorMessage);
						this.openDialog("Ocurrión un problema");
					}
				}
			);
		}
	
	}

	LoadTemplates(){
		this._PlantillaService.GetTemplates(this.idEmpresa).subscribe(
			result => {
				if(result.templates == 0){
					this.openDialog("No se encontró ninguna plantilla creada");
				}else{
					this.templates = result.resp;
				}
			},
			error => {
				this.errorMessage = <any>error;
				if(this.errorMessage != null){
					console.log(this.errorMessage);
					this.openDialog("Ocurrión un problema al cargar las plantillas");
				}
			}
		);
	}

	SelectTemplate(){
		// console.log(this.templateSelected);
		this.paso++;
		if(this.templateSelected > 0){
			this.getProceso(this.templateSelected);
		}
	}

	getProceso(idTemplate){
		console.log(idTemplate);
	}

	openDialog(texto): void {
		let dialogRef = this.dialog.open(DialogAlert, {
		  width: '350px',
		  data: texto//{ name: this.name, animal: this.animal }
		});

		// dialogRef.afterClosed().subscribe(result => {
		//   console.log('The dialog was closed');
		//   this.animal = result;
		// });
	}
	
	Retroceder(){
		this.paso--;
	}
}
