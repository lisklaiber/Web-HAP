import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

//import { AutenticationService } from '../services/autentication.service';
//import { Autentication } from '../models/autentication';

@Component({
	selector: 'add-user',
	templateUrl: '../views/add-user.html',
	styleUrls: ['../../assets/css/add-user.css', '../../assets/css/flujo.css'],
	providers: []
})

export class AddUserComponent implements OnInit{
	public paso: number;
	constructor(

	){
		this.paso = 1;
	}

	ngOnInit(){
		document.body.style.backgroundColor = "#FFF";
	}

	Avanzar(){
		this.paso++;
	}
	
	Retroceder(){
		this.paso--;
	}
}
