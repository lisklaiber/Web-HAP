import {Component, Inject} from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';


@Component({
  selector: 'dialog-alert',
  templateUrl: '../views/dialog-alert.html',
  styleUrls: ['../../assets/css/dialog-alert.css']
})
export class DialogAlert {

  	constructor(
		public dialogRef: MatDialogRef<DialogAlert>,
		@Inject(MAT_DIALOG_DATA) public data: any
	) { }

	okClick(): void {
		this.dialogRef.close();
	}

}