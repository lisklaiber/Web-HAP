import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

//import { AutenticationService } from '../services/autentication.service';
//import { Autentication } from '../models/autentication';

@Component({
	selector: 'edit-plant',
	templateUrl: '../views/edit-plantilla.html',
	styleUrls: ['../../assets/css/add-plantilla.css',
		'../../assets/css/add-user.css', 
		'../../assets/css/edit-plantilla.css', 
		'../../assets/css/edit-user.css'],
	providers: []
})

export class EditPlantillaComponent implements OnInit{
	public opc: number;
	constructor(
	){
		this.opc = 0;
	}

	ngOnInit(){
		document.body.style.backgroundColor = "#FFF";
	}
	Avanzar(){
		this.opc = 1;
	}
	Regresar(){
		this.opc = 0;
	}
}
