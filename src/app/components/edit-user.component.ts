import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

//import { AutenticationService } from '../services/autentication.service';
//import { Autentication } from '../models/autentication';

@Component({
	selector: 'edit-user',
	templateUrl: '../views/edit-user.html',
	styleUrls: ['../../assets/css/edit-user.css'],
	providers: []
})

export class EditUserComponent implements OnInit{
	public paso: number;
	constructor(

	){
		this.paso = 1;
	}

	ngOnInit(){
		document.body.style.backgroundColor = "#FFF";
	}

	Avanzar(){
		this.paso++;
	}
	
	Retroceder(){
		this.paso--;
	}
}
