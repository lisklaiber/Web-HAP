import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';


//import { AutenticationService } from '../services/autentication.service';
//import { Autentication } from '../models/autentication';

@Component({
	selector: 'struct-indic',
	templateUrl: '../views/estructura-indicadores.html',
	styleUrls: ['../../assets/css/estructura-indicadores.css'],
	providers: []
})

export class StructIndicComponent implements OnInit{
	public procesos: any = [];
	public i: number;
	public levels: any =[];
	public opc: number;
	public title: string;
	@Output() avanzarEvent = new EventEmitter<any>()

	constructor(){
		this.opc = 1;
		this.title = "Selecciona un elemento para editar";
	}
	
	ChangeOpc(){
		if(this.opc == 1){
			this.opc = 2;
			this.title = "Parametriza los indicadores";
		}else{
			this.opc = 1;
			this.title = "Selecciona un elemento para editar";

		}
	}

	ngOnInit(){
		
	}

	Avanzar(){
		this.avanzarEvent.emit();
	}

	
}