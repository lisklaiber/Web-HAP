import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

//import { AutenticationService } from '../services/autentication.service';
import { Proceso } from '../models/proceso';

@Component({
	selector: 'struct-proc',
	templateUrl: '../views/estructura-proceso.html',
	styleUrls: ['../../assets/css/add-plantilla.css', 
		'../../assets/css/flujo.css', 
		'../../assets/css/add-user.css', 
		'../../assets/css/edit-user.css',
		'../../assets/css/estructura-proceso.css'],
	providers: []
})

export class StructProcComponent implements OnInit{
	private procesos: Proceso[] = [];
	public i: number;
	public levels: any =[];
	private subIdProcesoPadre: number;
	private btnSubNivel: boolean;

	constructor(

	){
		this. i = 1;
		let nombreProc = "Proceso " + this.i;
		this.subIdProcesoPadre = 0;
		this.btnSubNivel = false;
		this.procesos.push(new Proceso(this.i, nombreProc, true, 1, "", 1, this.subIdProcesoPadre, new Date(), new Date(), 0, 0, 0));
		this.levels.push({Padre: this.subIdProcesoPadre, Hijo: this.i});
	}

	ngOnInit(){
		document.body.style.backgroundColor = "#FFF";
	}
	
	addProc(){
		this.i++;
		let nombreProc = "Proceso " + this.i
		this.procesos.push(new Proceso(this.i, nombreProc, true, 1, "", 1, this.subIdProcesoPadre, new Date(), new Date(), 0, 0, 0));
		this.levels.push({Padre: this.subIdProcesoPadre, Hijo: this.i});
		console.log("Procesos: ", this.procesos);
		console.log("levels: ", this.levels);
	}

	btnEditarClick(idProceso){
		this.subIdProcesoPadre = idProceso;
		this.ToogleBtnSubNivel();
	}

	btnRegresarClick(){
		for( let level of this.levels){
			if(level.Hijo == this.subIdProcesoPadre){
				this.subIdProcesoPadre = level.Padre;
				break;
			}
		}
		this.ToogleBtnSubNivel();
	}

	removeProc(index){
		let idProceso = this.procesos[index].Sub_ID;
		this.removeChilds(idProceso);
		this.procesos.splice(index, 1);
	}

	removeChilds(idProceso){
		// console.log(this.levels);
		let indices: number[] = [];

		for(let i = 0; i < this.procesos.length; i++){
			if(this.procesos[i].ID_Proc_Base == idProceso){
				indices.push(i);
			}
		}

		indices.forEach(indice =>{
			this.procesos.splice(indice, 1);	
		});

		indices = [];

		for(let i = 0; i < this.levels.length; i++){
			if(this.levels[i].Padre == idProceso){
				this.removeChilds(this.levels[i].Hijo);
				// i = 0;
			}
			if(this.levels[i].Hijo == idProceso){
				// indices.push(i);
				this.levels.splice(i, 1);
			}
		}
		
		// indices.forEach(indice =>{
		// 	this.levels.splice(indice, 1);
		// });
	}

	trackByprocesos(index, item) {
  		return item.id;
	}

	onlyUnique(value, index, self) { 
    	return self.indexOf(value) === index;
	}

	focusFunction(element){
		element.setSelectionRange(0, element.size);
		// console.log(element);
	}

	ToogleBtnSubNivel(){
		if(this.subIdProcesoPadre == 0){
			this.btnSubNivel = false;
		}else{
			this.btnSubNivel = true;
		}
	}
}