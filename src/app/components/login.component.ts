import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

import { AutenticationService } from '../services/autentication.service';
import { Autentication } from '../models/autentication';

@Component({
	selector: 'login',
	templateUrl: '../views/login.html',
	styleUrls: ['../../assets/css/login.css'],
	providers: [AutenticationService]
})

export class LoginComponent implements OnInit{
	public autentication: Autentication;
	public errorMessage: any;
	public resp: boolean;
	constructor(
		private _autenticationService: AutenticationService,
		private _route: ActivatedRoute,
		private _router: Router
	){
		
		this.resp = false;
	}

	ngOnInit(){
		this.autentication = new Autentication("", "");
		document.body.style.backgroundColor = "#641119";
	}
	onSubmit(){
		this._autenticationService.Autenticate(this.autentication).subscribe(
			result => {
				
				if(result.resp){
					this.resp = result.resp;
					this._router.navigate(['/proceso']);
					//alert(this.resp)
				}else{
					alert('Verifica tu usario y contraseña');
				}
			},
			error => {
				this.errorMessage = <any>error;
				if(this.errorMessage != null){
					console.log(this.errorMessage);
					alert("Usuario no encontrado");
				}
			}
		);
	}
}
