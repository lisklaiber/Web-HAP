import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

import { ProcesoViewService } from '../services/proceso-view.service';
//import { Autentication } from '../models/autentication';

@Component({
	selector: 'proceso-view',
	templateUrl: '../views/proceso-view.html',
	styleUrls: ['../../assets/css/Procesos.css'],
	providers: [ ProcesoViewService ]
})

export class ProcesoViewComponent implements OnInit{
	@Input() proceso: any;
	public procesosPrinc: any[] = [];
	public procesos: any[] = [];
	public levels: any[] = [];
	public serials: any[] = [];
	private errorMessage: string;
	constructor(
		private _ProcesoViewService: ProcesoViewService,
		private _route: ActivatedRoute,
		private _router: Router
	){
		
	}

	GetProceso(idProceso){
		this._ProcesoViewService.GetProceso(idProceso, 1).subscribe(
			result => {
				// console.log('result', result);

				if(!result){
					// alert(result.resp);
					console.log(result);
				}else{
					this.procesos = result.procesos;
					this.levels = result.levels;
					this.serials = result.serials;
					// console.log(this.procesos);
				}
			},
			error => {
				this.errorMessage = <any>error;
				if(this.errorMessage != null){
					console.log(this.errorMessage);
					// alert('Error en la peticion');
				}
			}
		);
	}

	GetProcPrin(idProceso){
		this._ProcesoViewService.GetProcPrin(idProceso, 1).subscribe(
			result => {
				// console.log('result', result);

				if(!result){
					// alert(result.resp);
					console.log(result);
				}else{
					this.procesosPrinc = result.resp;
					// console.log(this.procesos);
				}
			},
			error => {
				this.errorMessage = <any>error;
				if(this.errorMessage != null){
					console.log(this.errorMessage);
					// alert('Error en la peticion');
				}
			}
		);
	}

	ngOnInit(){
		// console.log(this.proceso);
		this.GetProcPrin(this.proceso.ID_Proceso);
	}
	
}
