export class Template{
	constructor(
		public ID_Plantilla: number,
		public Nombre: string,
		public Descripcion: string,
		public ID_Empresa: number
	){}
}