export class Proceso{
	constructor(
		public Sub_ID: number,
		public Nombre: string,
		public Obligatorio: boolean,
		public Tipo: number,
		public Descripcion: string,
	    public ID_Empresa: number,
		public ID_Proc_Base: number,
		public Fecha_Ini: Date,
		public Fecha_Fin: Date,
		public Duracion: number,
		public Completado: number,
		public Costo: number
	){
	}

}