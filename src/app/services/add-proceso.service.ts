import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { Proceso } from '../models/proceso';

@Injectable()

export class AddProcesoService{
	public url: string;

	constructor(private _Http: Http){
		this.url = 'http://localhost:5000/api/';
	}

	ValidarProceso(nombre, idEmpresa): Observable<any>{
		return this._Http.get(`${this.url}ValidarProceso/${nombre}/${idEmpresa}`).map(res => res.json());
	}
}