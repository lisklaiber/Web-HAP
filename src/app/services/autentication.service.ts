import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http'
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { Autentication } from '../models/autentication'

@Injectable()

export class AutenticationService{
	public url: string;
	constructor(private _http: Http){
		this.url = 'http://localhost:5000/api/'
	}

	Autenticate(autentication: Autentication){
		let json = JSON.stringify(autentication);
		let params = json;
		let headers = new Headers({'Content-Type': 'application/json'});

		return this._http.post(this.url + 'autentication', params, {headers: headers})
			.map(res => res.json())
	}

}