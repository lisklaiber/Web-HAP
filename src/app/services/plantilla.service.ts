import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';

@Injectable()

export class PlantillaService{
	public url: string;

	constructor(private _Http: Http){
		this.url = `http://localhost:5000/api/`;
	}

	GetTemplates(idEmpresa): Observable<any>{
		return this._Http.get(`${this.url}getPlantillas/${idEmpresa}`).map(res => res.json());
	}
}