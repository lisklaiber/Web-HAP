import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http'
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
// import { Autentication } from '../models/autentication';


@Injectable()

export class ProcesoService{
	public url: string;
	constructor(private _http: Http){
		this.url = 'http://localhost:5000/api/'
	}

	GetProcesos(): Observable<any>{
		return this._http.get(this.url + 'GetProcesos/').map(res => res.json());
	}
}